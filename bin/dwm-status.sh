#!/bin/sh

while xsetroot -name "$status_date Key: $status_keyboard Vol: $status_volume APM: $status_apm Batt: $status_battery_percent ($status_battery_time) [$status_power_supply]"
do
	sleep 1

    	status_battery_percent=`apm|head -n 1|cut -f4 -d" "`
	status_battery_time=`apm|head -n 1|cut -f6,7 -d" "`
	status_date=`date "+%a %b %e %H:%M:%S %p"`
	status_keyboard=`setxkbmap -query | sed -rn 's/layout.*(..)/\1/p'`
	status_apm=`apm | tail -n 1 | cut -f4-6 -d" "`
	status_volume=$(( `mixerctl outputs.master | sed -rn 's/outputs.master=(...?),(...)?/\2/p'` * 100 / 255 ))%
	status_power_supply=`if [[ \`sysctl -n hw.sensors.acpiac0.indicator0\` == On* ]]; then echo "AC"; fi`
done
