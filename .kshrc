:
. /etc/ksh.kshrc

PS1="${USER}@`hostname -s`:\${PWD##*/}\$ "

case "$TERM" in
xterm*|rxvt*|urxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac


alias vi="vim"

#set cp
#set compatibility
